document.getElementById('submit').onclick = function () {
  document.getElementById("submit").remove();
  let round = document.getElementById("round");
  let inputSize = document.getElementById('txt');
  let inputColor = document.getElementById('color');
  let createBtn = document.getElementById('button');
  let alert = document.getElementById('alert');
  alert.style.display = 'none';
  document.getElementById("hidden").style.display = 'block';

  document.getElementById('button').onclick = function(e) {
    e.preventDefault();
    inputSizeValue = Number(inputSize.value);
    inputColorValue = inputColor.value;
    if (isNaN(inputSizeValue) || inputSizeValue == 0 || inputColorValue == 0 || typeof(inputColorValue) == 'String') {
      alert.style.display = 'block';
      alert.style.padding = '4px';
      alert.style.borderRadius = '4px';
      alert.style.fontFamily = 'Verdana,sans-serif';
      alert.style.fontSize = '15px';
      alert.style.color = 'white';
      alert.style.backgroundColor = '#f44336';
    } else {
      alert.style.display = 'none';
    }
    let newСircle = document.createElement('div');
    newСircle.className = 'newСircle';
    newСircle.style.marginRight = '20px';
    newСircle.style.marginBottom = '20px';
    newСircle.style.WebkitBorderRadius = '50%';
    newСircle.style.float = 'left';
    newСircle.style.height = inputSizeValue + 'px';
    newСircle.style.width = inputSizeValue + 'px';
    newСircle.style.backgroundColor = inputColorValue;
    round.appendChild(newСircle);
  };
};
