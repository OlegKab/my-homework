## Задание

Реализовать программу на Javascript, 
которая будет находить все простые числа в заданном диапазоне.

#### Технические требования:
- Считать с помощью модального окна браузера число, которое введет пользователь. 
- Вывести в консоли все [простые числа]
(https://ru.wikipedia.org/wiki/%D0%9F%D1%80%D0%BE%D1%81%D1%82%D0%BE%D0%B5_%D1%87%D0%B8%D1%81%D0%BB%D0%BE) 
от 1 до введенного пользователем числа.
- Обязательно необходимо использовать синтаксис ES6 (ES2015) при создании переменных.

#### Не обязательное задание продвинутой сложности:
- Проверить, что введенное значение является целым числом, и больше единицы. 
Если данные условия не соблюдаются, повторять вывод окна на экран до тех пор, 
пока не будет введено целое число больше 1.
- Максимально оптимизировать алгоритм, чтобы он выполнялся как можно 
быстрее для больших чисел.
- Считать два числа, `m` и `n`. 
Вывести в консоль все простые числа в диапазоне от `m` до `n` 
(меньшее из введенных чисел будет `m`, бОльшее будет `n`). 
Если хотя бы одно из чисел не соблюдает условия валидации, 
указанные выше, вывести сообщение об ошибке, и спросить оба числа заново. 

#### Литература:
- [Циклы while, for](https://learn.javascript.ru/while-for)






let firstNumber = checkNumber(Number(prompt("Enter the first number", "")));
let secondNumber = checkNumber(Number(prompt("Enter the second number", "")));

function checkNumber (number) {
  while (parseInt(number) !== number || number <= 1) {
    number = prompt("Enter the natural number that bigger than 1", "");
    if (number === null) break;
  }
  return number;
}

function checkPrimeNumber (start, finish) {
  let arr = [];
  for (let i = start; i <= finish; i++) {
    for (let j = 2; j < i; j++) {
      if (i % j === 0) {
        j = i;
        continue;
      }
      if ((j+1) === i) {
        arr.push(i);
      }
    }

  }
  return arr.join(', ');
}
console.log(checkPrimeNumber(firstNumber, secondNumber));
