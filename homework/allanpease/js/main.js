	jQuery(document).ready(function(){
	if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
		$('html').addClass('iOS-device');

	}

		$('.click-m').click(function(e){
			e.preventDefault();
			var th = $(this);
			th.nextAll().addClass('vis');
			setTimeout(function(){
				th.nextAll().removeClass('vis');
			}, 3000);

      ga('send', 'event', 'tickets', 'click', 'buy');
		});







		$('.owl-carousel').owlCarousel({
			loop:true,
			margin:30,
			nav:false,
			dotsContainer: '.testimonials .owl-dots-custom',
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:2
				}
			}
		});

		$('.testimonials .next').click(function() {
			$('.owl-carousel').trigger('next.owl.carousel');
		})
		// Go to the previous item
		$('.testimonials .prev').click(function() {
			$('.owl-carousel').trigger('prev.owl.carousel');
		})

		$('.about .nav .next').click(function(e) {
			e.preventDefault();
			$('.about .owl-carousel').trigger('next.owl.carousel');
		})
		$('.about .nav .prev').click(function(e) {
			e.preventDefault();
			$('.about .owl-carousel').trigger('prev.owl.carousel');
		})



		$('.owl-partners').owlCarousel({
			loop:true,
			margin:30,
			nav:true,
			responsive:{
				0:{
					items:2
				},
				600:{
					items:4
				},
				1000:{
					items:5
				}
			}
		});



		$('.testimonials-carousel').owlCarousel({
			loop:true,
			nav:true,
			items: 1
		});







	var height = screen.height;
		$('.play')
			.attr('rel', 'media-gallery')
			.fancybox({
			width : 4/3. * height,
			height : height,
			autoDimensions : false,
			openEffect : 'none',
			closeEffect : 'none',
			prevEffect : 'none',
			nextEffect : 'none',
			padding: 0,
			arrows : false,
			helpers : {
				media : {},
				buttons : {},
				overlay: {
				  locked: false
				}
			}
		});


var day_z = new Date("05 September 2018 00:01"); // January, February, March, April, May, June, July, August, September, October, November, December

function timer() {
  var time_now = new Date(),
    time_delta = day_z - time_now;
  if (time_delta < 0) {
    time_delta = 0;
  }
  var time_day = Math.floor(time_delta / 86400000),
    time_ost = time_delta - Math.floor(time_delta / 86400000) * 86400000,
    time_hrs = Math.floor(time_ost / 3600000);
  time_ost = time_ost - Math.floor(time_ost / 3600000) * 3600000;
  var time_min = Math.floor(time_ost / 60000);
  time_ost = time_ost - Math.floor(time_ost / 60000) * 60000;
  var time_sec = Math.floor(time_ost / 1000);
  if (time_day < 10) {
    time_day = '0' + time_day;
  }
  if (time_hrs < 10) {
    time_hrs = '0' + time_hrs;
  }
  if (time_min < 10) {
    time_min = '0' + time_min;
  }
  if (time_sec < 10) {
    time_sec = '0' + time_sec;
  }
  $('.timer .sec ').text(time_sec);
  $('.timer .hours ').text(time_hrs);
  $('.timer .days ').text(time_day);
  $('.timer .mins ').text(time_min);
  if (time_delta == 0) {
    clearInterval(akcia_interval);
	$('.timer').addClass('over');
  }
  //alert(time_day.toString().length);
  var dney = time_day.toString().length;
  if (dney == 3) {
    //$('.days strong').addClass('three');

  } else if (dney == 2) {
   // $('.days strong').addClass('two')
  }

}

// timer run
 var akcia_interval = setInterval(timer, 1);


		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 1000);
				return false;
			  }
			}
		  });
		});



    jQuery.validator.addMethod("digits", function(value, element) {
        return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
    });

    jQuery.validator.addMethod("letters", function(value, element) {
        return this.optional(element) || /^[а-яА-ЯёЁa-zA-Z_\s]+$/i.test(value);
    });

    $("#form").validate({

        rules:{

            name:{
                required: true,
                letters:true,
                minlength: 2
            },

            email:{
                required: true,
                email:true
            },

            phone:{
                required: true,
                digits: true,
                minlength: 4,
            }
        },

        errorPlacement: function(error, element) {
            return true;
        },
        submitHandler: function() {
            var action = $('#form').attr('action'),
                post = $('#form').serializeArray(),
                sendmessage_text='Идет регистрация...';
            $.ajax({
                url: action,
                type: 'post',
                data: post,
                dataType: 'json',
                beforeSend: function() {

                    $('body').append('<div class="sendmessage-overlay"><div class="content"><i></i><br /> '+sendmessage_text+'</div></div>');
                    $('.sendmessage-overlay').fadeIn(150);

                },
                success: function(data) {
                    if(data.location) {
                        window.location.href=data.location;
                    }
                },
                complete: function() {
                    $('.sendmessage-overlay').fadeOut(150, function() {
                        $(this).remove();
                    });
                }
            });
        }

    });






	});


$(document).on('closing', '.remodal', function (e) {

	$('input[type="text"]').val('');
	$('textarea').val('');
});





$(document).on('opened', '.iOS-device .remodal', function () {
	lockScroll();
});

$(document).on('closed', '.iOS-device .remodal', function (e) {
	unlockScroll();
	$('body').removeAttr('style');
});

var body = document.getElementsByTagName('body')[0];
var bodyScrollTop = null;
var locked = false;

// Заблокировать прокрутку страницы
function lockScroll(){
	if (!locked) {
		bodyScrollTop = (typeof window.pageYOffset !== 'undefined') ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
		body.classList.add('open');
		body.style.top = '-${bodyScrollTop}px';
		locked = true;
	};
}

// Включить прокрутку страницы
function unlockScroll(){
	if (locked) {
		body.classList.remove('open');
		body.style.top = null;
		window.scrollTo(0, bodyScrollTop);
		locked = false;
	}
}


function initialize() {
    var myLatlng = new google.maps.LatLng(50.454763, 30.509677);
    var mapOptions = {
        zoom: 16,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false,
          zoomControl: true,
          zoomControlOptions: {
              position: google.maps.ControlPosition.LEFT_CENTER
          },
		styles: [
    {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    }
]
    }
    var map = new google.maps.Map(document.getElementById('map'), mapOptions);

     //=====Initialise Default Marker
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Киев, Kiev Chamber Plaza'
     //=====You can even customize the icons here
    });

     //=====Initialise InfoWindow
    var infowindow = new google.maps.InfoWindow({
      content: "<span style='color: #000;'>Киев, Kiev Chamber Plaza</span>"
  });

   //=====Eventlistener for InfoWindow
  google.maps.event.addListener(marker, 'click', function() {
    infowindow.open(map,marker);
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

var wow = new WOW(
  {
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
      // the callback is fired every time an animation is started
      // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
  }
);
wow.init();
