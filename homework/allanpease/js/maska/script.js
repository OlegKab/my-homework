
var maskList = $.masksSort($.masksLoad("js/maska/phone-codes.json"), ['#'], /[0-9]|#/, "mask");

	var maskOpts = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			//clearIncomplete: true,
			showMaskOnHover: true,
			autoUnmask: true
		},
		match: /[0-9]/,
		replace: '#',
		list: maskList,
		listKey: "mask",
		onMaskChange: function(maskObj, completed) {
			
			$(this).attr("placeholder", $(this).inputmask("getemptymask"));
		}
	};
$('input[name=phone]').inputmasks(maskOpts).val('7');

$(document).on('closing', '.remodal', function (e) {
	$('input[name=phone]').inputmasks(maskOpts).val('7');
});
        

           $('.grInp').on('input', function() {
   var len = $(this).val().length;
   if(len >= 1){
	  $(this).removeClass('grInp').addClass('blInp'); 
   }
   else{
	  $(this).removeClass('blInp').addClass('grInp'); 
   }
});