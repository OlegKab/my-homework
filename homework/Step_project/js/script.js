$(function () {

  $("#wr-tabs").on("click", ".services-menu", function () {

    var tabs = $("#wr-tabs .services-menu"),
      cont = $("#wr-tabs .services-menu-select");

    // Удаляем классы active
    tabs.removeClass("active");
    cont.removeClass("active");
    // Добавляем классы active
    $(this).addClass("active");
    cont.eq($(this).index()).addClass("active");
    return false;
  });
});
/**************************************/
/*кнопка загрузки больше*/

// $( document ).ready(function () {
//   $(".work-carusel-img").slice(0, 12).show();
//   if ($(".hident:hidden").length != 0) {
//     $("#loadMore").show();
//   }
//   $("#loadMore").on('click', function (e) {
//     e.preventDefault();
//     $(".work-carusel-img:hidden").slice(0, 12).slideDown();
//     if ($(".work-carusel-img:hidden").length == 0) {
//       $("#loadMore").fadeOut('slow');
//     }
//   });
// });



$(function(){
  $(".work-menu a").click(function(e){ // click event for load more
    e.preventDefault();
    let categoryName = $(this).attr("data-info");
    $(".work-carusel-img").addClass("hidden");
    $(".work-carusel-img."+categoryName).removeClass("hidden");
  });
  $(".work-carusel-img").slice(0, 0).show(); // select the first ten
  $("#loadMore .work-btn").click(function(e){ // click event for load more
    e.preventDefault();
    $(".work-carusel-img.hidden").removeClass("hidden");
          $("#loadMore").fadeOut('slow');
  });

  $('.item-masonry').hover(
    function () {
      $(this).find(".cover-item-gallery").fadeIn();
      $(this).find(".fas-btn").fadeIn();
    },
    function () {
      $(this).find(".cover-item-gallery").fadeOut();
      $(this).find(".fas-btn").fadeOut();
    }
  );


  var elem = document.querySelector('#gallery');
  var msnry = new Masonry( elem, {
    // options
    itemSelector: '.item-masonry',
    columnWidth:390
  });


});


$( document ).ready(function () {
  $(".item-masonry").slice(0, 0).show();
  if ($(".item-masonry:hidden").length != 0) {
    $("#loadMore").show();
  }
  $("#loadMoreMasonry").on('click', function (e) {
    e.preventDefault();
    $(".item-masonry:hidden").slice(0, 6).slideDown();
    if ($(".item-masonry:hidden").length == 0) {
      $("#loadMoreMasonry").fadeOut('slow');
    }
    var elem = document.querySelector('#gallery');
    var msnry = new Masonry( elem, {
      // options
      itemSelector: '.item-masonry',
      columnWidth:390
    });
  });
});


/**********************************************/


//Обработка клика на стрелку вправо
$('body').on('click', ".review-carusel-btn-right",function(){
  var carusel = $(this).parents('.carousel');
  right_carusel(carusel);
  return false;
});

//Обработка клика на стрелку влево
$('body').on('click',".review-carusel-btn-left",function(){
  var carusel = $(this).parents('.carousel');
  left_carusel(carusel);
  return false;
});
function left_carusel(carusel){
  var block_width = $(carusel).find('.carousel-block').outerWidth();
  $(carusel).find(".carousel-items .carousel-block").eq(-1).clone().prependTo($(carusel).find(".carousel-items"));
  $(carusel).find(".carousel-items").css({"left":"-"+block_width+"px"});
  $(carusel).find(".carousel-items .carousel-block").eq(-1).remove();
  $(carusel).find(".carousel-items").animate({left: "0px"}, 200);

}
function right_carusel(carusel){
  var block_width = $(carusel).find('.carousel-block').outerWidth();
  $(carusel).find(".carousel-items").animate({left: "-"+ block_width +"px"}, 200, function(){
    $(carusel).find(".carousel-items .carousel-block").eq(0).clone().appendTo($(carusel).find(".carousel-items"));
    $(carusel).find(".carousel-items .carousel-block").eq(0).remove();
    $(carusel).find(".carousel-items").css({"left":"0px"});
  });
}

$(function() {
//Раскомментируйте строку ниже, чтобы включить автоматическую прокрутку карусели
//	auto_right('.carousel:first');
});

// Автоматическая прокрутка
function auto_right(carusel){
  setInterval(function(){
    if (!$(carusel).is('.hover'))
      right_carusel(carusel);
  }, 1000)
}
// Навели курсор на карусель
$(document).on('mouseenter', '.carousel', function(){$(this).addClass('hover')})
//Убрали курсор с карусели
$(document).on('mouseleave', '.carousel', function(){$(this).removeClass('hover')})


/******************************************************************/
//появление верхнего блока


$(function(){
  $("body").on("click",".foto-carusel-img", function(e){ // click event for load more
    e.preventDefault();
    let categoryName = $(this).attr("data-info");
    $(".review-block").addClass("hidd");
    $(".review-block."+categoryName).removeClass("hidd");
  });


  $("body").on("click", ".carousel-block", function(c){
    $(this).addClass('inaction').siblings().removeClass('inaction')

  })
});















