// Write a function createCalendar(elem, year, month)
// // The call should create a calendar for the given year/month and put it inside elem
// // The calendar should be a table, where a week is <tr>, and a day is <td>
// // The table top should be <th> with weekday names
// // For instance, createCalendar(cal, 2018, 6) should generate in element cal the following calendar:
/*
/*
Function steps:
1. Create table structure
1.1. Get nubmer of weeks in the month
2. Fill table with data"
3. Insert table in the end of the element
*/

function createCalendar (elem, year, month) { // adds calendar to the end of the element as a table. Year and month must be given as a number.
  elem.innerHTML = '';
  let table = document.createElement("table"); //создает таблицу
  let caption = document.createElement("caption");
  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
  ];
  caption.innerHTML = monthNames[month-1] + ' / ' + year;
  table.appendChild(caption);
  table.classList.add("calendar"); // добавляет таблице класс
  let thead = document.createElement("thead"); //создает заголовок таблицы
  let days = ["SU", "MO", "TU", "WE", "TH", "FR", "SA"]; // массив для заголовка таблицы, дни



  for (let i =0; i<7; i++) { // цикл формирования заголовка календаря
    let th = document.createElement("th");
    th.innerHTML = days[i];
    thead.appendChild(th);
  }
  table.appendChild(thead); // добавляет заголовок в таблицу


  let tbody = document.createElement("tbody"); // создает тело таблицы

  let d = new Date(year, month , 0); //получает количество дней в месяце
  let daysInMonth = d.getDate(); // получает количество дней в месяце
  let weeks = Math.ceil(daysInMonth/7); //получает количество недель
  let day = 1;
  let firstDay = new Date(year, (month - 1), 1) ; // получение первого дня недели в месяце
  firstDay = firstDay.getDay(); // получение первого дня недели




  for (let i =0; i<weeks; i++) { // цикл формирования таблицы, понедельно
    let tr =  document.createElement ("tr");
    for (let j =0; j<7; j++) {    // цикл формирования таблицы, по дням
      let td =  document.createElement ("td");
      if (i===0 && j < firstDay){ // определение с какого дня начинать записывать значения
        tr.appendChild(td);
        continue;
      }
      else {
        if (day<=daysInMonth ) {     // определние до какого дня записыватьь значения
          td.innerHTML = day;
        }
      }
      tr.appendChild(td);
      day++;
    }
    tbody.appendChild(tr);
  }

  table.appendChild(tbody); // добавление тела таблицы в таблицу
  elem.appendChild(table); // добавление таблицы в элемент

}

let elem = document.getElementById("calendar"); // присваивание переменной elem объекта с id calendar

let date = new Date; // переменная месяца
let yearInput = date.getFullYear(); //переменная года
let monthInput = (date.getMonth())+1;
console.log(monthInput);

createCalendar(elem, yearInput, monthInput ); // вызов функции для формирования календаря

function prevMonth() {
  --monthInput;
  if (monthInput === 0){
    monthInput = 12;
    --yearInput;
  }
  createCalendar(elem, yearInput, monthInput );
}

function nextMonth() {
  ++monthInput;
  if (monthInput === 13){
    monthInput = 1;
    ++yearInput;
  }
  createCalendar(elem, yearInput, monthInput );
}

btnLeft.addEventListener("click", prevMonth);
btnRight.addEventListener("click", nextMonth);





// <script>
// function createCalendar(id, year, month) {
//   var elem = document.getElementById(id);
//
//   var mon = month - 1; // месяцы в JS идут от 0 до 11, а не от 1 до 12
//   var d = new Date(year, mon);
//
//   var table = '<table><tr><th>пн</th><th>вт</th><th>ср</th><th>чт</th><th>пт</th><th>сб</th><th>вс</th></tr><tr>';
//
//   // заполнить первый ряд от понедельника
//   // и до дня, с которого начинается месяц
//   // * * * | 1  2  3  4
//   for (var i = 0; i < getDay(d); i++) {
//     table += '<td></td>';
//   }
//
//   // ячейки календаря с датами
//   while (d.getMonth() == mon) {
//     table += '<td>' + d.getDate() + '</td>';
//
//     if (getDay(d) % 7 == 6) { // вс, последний день - перевод строки
//       table += '</tr><tr>';
//     }
//
//     d.setDate(d.getDate() + 1);
//   }
//
//   // добить таблицу пустыми ячейками, если нужно
//   if (getDay(d) != 0) {
//     for (var i = getDay(d); i < 7; i++) {
//       table += '<td></td>';
//     }
//   }
//
//   // закрыть таблицу
//   table += '</tr></table>';
//
//   // только одно присваивание innerHTML
//   elem.innerHTML = table;
// }
//
// function getDay(date) { // получить номер дня недели, от 0(пн) до 6(вс)
//   var day = date.getDay();
//   if (day == 0) day = 7;
//   return day - 1;
// }
//
//
//
// createCalendar("calendar", 2012, 9)
// </script>





