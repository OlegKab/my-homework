/*
let obj = {
  name: "Andrey",
  age: 25,
  "some obj": 10
};
// читать что в обїекте
obj.age  // явное задание свойств
obj["age"] // обращение когда неизвеста переменная

// изменение обьекта
obj.age = 20;
obj["age"] = 20;

//удаление объекта
delete obj.age;

*/


// let obj = {
//   name: "Jhon",
//   age: "25",
//   sex: "male'"
// };
// console.log(obj);
// console.log(obj.name);
// delete obj.sex;
// console.log(obj);
//
// obj.age = 50;
// console.log(obj.age);
// console.log(obj);

//********************************************************
//
// let obj = {
//   someFun: function (num1, num2) {
//     return num1 + num2;
//   }
// };
// let res = obj.someFun(1,2);


//********************************************************
// проверка на существование объекта
//
// if ("age" in obj){
//   console.log(true)
// }


//*********************************************************

// let obj2 = {
//   one: 1,
//   two: 2,
//   three: 3,
//   four: 4,
//   five: 5,
// };
//
// let count = 0;
// for (let key in obj2) {
// count += obj2[key];
// }
// console.log(count);

//*************************************************************


// let user = {name: "Ivan"};
// let admin = user;   // обращаемся к облости памяти, при изменении значения у   user  будет измененно и у  admin  -  так же и обратно
//
// let userList ={
//   user1: {},
//   user2: {},
//   user3: {},
// };
//
// let currentUser = userList.user2;


//***************************************************************
// сравнение двух объектов всегда будет фолс

// let a = {};
// let b = {};
// let c = a;
//
// console.log(c === a);
// console.log(b == a);

//*****************************************************************
//кланирование/кланирование объекта

// let a = {some: 123};
// let b = Object.assign({},a);
//
// console.log(b);

//******************************************************************
//  объект this

// console.log(this);
//
// let obj = {
//   name: "Ivan",
//   secondName: "Petrovich",
//   fullName: "",
//
//   generateFullName: function () {
//     this.fullName = this.name + this.secondName
//     console.log(this.fullName);
//   }
// };
// obj.generateFullName();
// console.log(obj.fullName);
// console.log(this.fullName);

//*********************************************************************
//  не должно работать НО работает

// let obj = {
//   name: "Ivan",
//   secondName: "Petrovich",
//   fullName: "",
//
//   generateFullName: function () {
//     this.fullName = obj.name + obj.secondName;
//     console.log(obj.fullName);
//   }
// };
// obj.generateFullName();
// console.log(obj.fullName);
// console.log(this.fullName);


//*************************************************************************
//  техническая пустая строка
// возникает при обращении с к облости видимости без объекта

// function makeUser() {
//   return{
//     name: "Jhon",
//     ref: this,
//   };
// }
//
// let user = makeUser();
// console.log(typeof (user.ref.name));



//****************************************************************************

let ladder = {
  step: 0,
  up: function() {
    this.step++;
    return this;
  },
  down: function(){
    this.step--;
    return this;
  },
    showStep: function () {
       console.log(this.step);
  }
};

ladder.up();  // выводин построчно
ladder.up();
ladder.down();
ladder.showStep();


ladder.up().up().down().showStep();   // выводим одной строкой, не забывать делать "return"


//************************************************************************************



























