
// let blueItems = document.body.querySelectorAll('*[title="Blue"]');
//
// for (let i = 0; i < blueItems.length; i++){
//   blueItems[i].style.backgroundColor = "red";
//
// }

//*********************************************************


// $("a[title*='Blue']").css("color", "blue"); // *=  означает содержит


//**********************************************************

// 1. Есть кнопка и скрытый текст рядом. При клике на кнопку текст рядом плавно появляется.
//   Анимацию появления сделай тоже через jquery.
// 2. При скролле страницы(через пару скроллов) показывать справа-внизу кнопку "наверх".
//   При клике на нее - страница плавно скроллится в начало и кнопка прячется до следующего раза.
// 3. Всплывающее окно. У тебя есть сверстанное окно поверх страницы и оно спрятано.
//   И есть кнопка, при клике на которую это окно появляется. У окна есть крестик, при клике на который окно прячется.
//   Также при клике на esc на клавиатуре окно тоже должно прятаться.

// 1.
// способ_1
// $(document).ready(function(){
//
//   $("#showHideContent").click(function () {
//     if ($("#content").is(":hidden")) {
//
//       $("#content").show("slow");
//
//     } else {
//
//       $("#content").hide("slow");
//
//     }
//     return false;
//   });
// });

//способ_2

// $('#showHideContent').click(function(){
//   $('#content').slideToggle(1000);
//   return false;
// });


//способ_3 в класе
// $( document ).ready(function() {
//   $("#content").hide();
//
//   $('#showHideContent').click( function () {
//     $("#content").show(1000);
//   });
// });


// 2.

//
  // $(function () {
  //   $('#scrollToBottom').bind("click", function () {
  //     $('html, body').animate({scrollTop: $(document).height() }, 2000);
  //     return false;
  //   });
  //   $('#scrollToTop').bind("click", function () {
  //     $('html, body').animate({ scrollTop: 0 }, 2000);
  //     return false;
  //   });
  // });

// вариант в класе
// .scrollToTop{
//   width:100px;
//   height:130px;
//   padding:10px;
//   text-align:center;
//   background: whiteSmoke;
//   font-weight: bold;
//   color: #444;
//   text-decoration: none;
//   position:fixed;
//   top:75px;
//   right:40px;
//   display:none;
//   background: url('arrow_up.png') no-repeat 0px 20px;
// }
// .scrollToTop:hover{
//   text-decoration:none;
// }
// .text {
//   margin: 0 auto;
//   width: 50px;
// }
// <!DOCTYPE html>
// <html lang="en">
//   <head>
//   <meta charset="UTF-8">
//   <link rel="stylesheet" href="css/stylesscrollbutton.css">
//   <title>Document</title>
//   </head>
//   <body>
//   <p class="text">
//   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nisi veniam quos aperiam, omnis sit deleniti, hic quod adipisci excepturi illum cum nulla error voluptates unde cupiditate itaque cumque non.
//   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aperiam aspernatur, tempore expedita tenetur ipsa quam quidem in, nam voluptatum alias, consequatur beatae maxime! Consectetur iste eveniet modi ipsam, minima!
//   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi adipisci, error, est omnis fugit eum consequuntur. Neque, excepturi. Placeat asperiores qui quasi, praesentium eius ratione labore mollitia, quaerat minima tenetur.
// </p>
// <a href="#" class="scrollToTop">Scroll To Top</a>
//
// <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
//   <script src=js/script.js></script>
//   </body>
//   </html>

// $(document).ready(function(){
//
//   //Check to see if the window is top if not then display button
//   $(window).scroll(function(){
//     if ($(this).scrollTop() > 100) {
//       $('.scrollToTop').fadeIn();
//     } else {
//       $('.scrollToTop').fadeOut();
//     }
//   });
//
//   //Click event to scroll to top
//   $('.scrollToTop').click(function(){
//     $('html, body').animate({scrollTop : 0},800);
//     return false;
//   });
//
// });

//*******************************************************************


//3

$(document).ready(function(){
  $('.modal').hide();
  $('.open-modal').on('click', function(){
    let getHref = $(this).attr('href');
    $(getHref).show(1000);
  });
});
















