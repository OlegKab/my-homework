// Create a web page that asks the user to enter a year, and prints whether this year is  a leap year
// Every year that is exactly divisible by four is a leap year, except for years that are
// exactly divisible by 100, but these centurial years are leap years if they are exactly  divisible by 400
// For example, the years 1700, 1800, and 1900 were not leap years, but the years 1600  and 2000 were

function isNumber(year) {
  if (isNaN(Number(year)) ||
    year === null ||
    !parseInt(year) ||
    Number(year) <= 0) {
    return false;
  }
  return true;
}

function isLeapYear(year) {
  if (year % 4 === 0) {
    if (year % 100 === 0 && year % 400 !== 0) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }

}

let year = prompt("Input year", "");

if (!isNumber(year)) {
  alert("Введенное число не является корректным");
} else {
  let isYear = isLeapYear(year);
  if (isYear) {
    alert("Високосный год!");
  } else {
    alert("Не високосный год");
  }
}

console.log(isLeapYear(year));
