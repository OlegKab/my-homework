// при введении года что бы выдавала когда Псха
/* 1. Ввести год
 * 2. Проверить введено ли число

Для определения даты Православной пасхи по старому стилю необходимо:

Разделить номер года на 19 и определить остаток от деления a.
Разделить номер года на 4 и определить остаток от деления b.
Разделить номер года на 7 и определить остаток от деления c.
Разделить сумму 19a + 15 на 30 и определить остаток d.
Разделить сумму 2b + 4c + 6d + 6 на 7 и определить остаток e.
Определить сумму f = d + e.
(по новому стилю) Если f ≤ 26, то Пасха будет праздноваться 4 + f апреля; если f > 26, то Пасха будет праздноваться f — 26 мая.
Более сложный алгоритм расчёта Католической пасхи покажем на примере.

*/

function isNumber(easterYear) {
  while (easterYear == null || isNaN(Number(easterYear)) || Number(easterYear) <= 33 || Number.isInteger(easterYear)) {

    if (easterYear == null) {
      easterYear = prompt("Введите год повторно!", "");
      continue;
    }
    if (isNaN(Number(easterYear))) {
      easterYear = prompt("Введите год в формате числа!", "");
      continue;
    }
    if  (Number(easterYear) <= 33) {
      easterYear = prompt("Введите год больше 33 года!", "");
    }
  }
  return Number(easterYear);

}

let easterYear = prompt("Введите год, в который вы хотите узнать дату Пасхи", "");
easterYear = isNumber(easterYear);


easterDateA = easterYear % 19;
easterDateB = easterYear % 4;
easterDateC = easterYear % 7;
easterDateD = Math.round((19 * easterDateA + 15) % 30);
easterDateE = Math.round((2 * easterDateB + 4 * easterDateC + 6 * easterDateD + 6) % 7);
easterDateF = easterDateD + easterDateE;


if (easterDateF <= 26){
  easterDate = 4 + easterDateF;
  alert("Пасха в " + easterYear + " году поподаета на " + easterDate + " аплеля ")
}else {
  easterDate = easterDateF - 26;
  alert("Пасха в " + easterYear + " году поподаета на " + easterDate + " мая ")
}

//
// console.log(easterDateA)
// console.log(easterDateB)
// console.log(easterDateC)
// console.log(easterDateD)
// console.log(easterDateE)
// console.log(easterDateF)
