// Let arr be an array
// Create a function unique(arr) that should return an array with unique items of arr
// Use set to make the function more efficient
// For instance:

//
// function unique(arr) {
//   let resultArr = [];
//   for (let i = 0; i < arr.length; i++){
//     let value = arr[i];
//
//     if (resultArr.indexOf(value) < 0 ){
//         resultArr.push(arr[i]);
//     }
//   }
//
// return resultArr;
//
// }
// let values = ["John", "Harry", "Mary", "Harry", "Beth", "Harry", "Mary", "John"];
// alert(unique(values)); // John, Harry, Mary, Beth


// second


// function unique(arr) {
//   let resultArr = new Set();
//   for (let i = 0; i < arr.length; i++){
//     resultArr.add(arr[i]);
//   }
//   return resultArr;
//
// }
// let values = ["John", "Harry", "Mary", "Harry", "Beth", "Harry", "Mary", "John"];
// console.log(unique(values)); // John, Harry, Mary, Beth



// therd   set

function unique(arr) {
  return new Set(arr);
}

let values = ["John", "Harry", "Mary", "Harry", "Beth", "Harry", "Mary", "John"];
console.log(unique(values)); // John, Harry, Mary, Beth
console.log(new Set(values)); // John, Harry, Mary, Beth




//  самый короткий вариант вывода

// let values = ["John", "Harry", "Mary", "Harry", "Beth", "Harry", "Mary", "John"];
// console.log(new Set(values)); // John, Harry, Mary, Beth
