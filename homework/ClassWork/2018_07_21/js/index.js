// let product = {};
// product.name = "leptop";
// product.price = 1200;
// product.price = 1000;
//
// console.log(product.name);
// console.log(product.price);
//
// delete product.name;
// console.log(product);


//***************************************************************
// Create an object calculator with three methods:  read
// () prompts for two values and saves them as object properties  sum() returns the sum of saved values  mul()
// multiplies saved values and returns the result


// let calculator = {
//   firstValue: 20,
//   secondValue: 30,
//   sumValue: this.secondValue + this.firstValue, // так значение не выводится
//   mulValue: "",
//
//   read: function() {
//     this.firstValue = Number(prompt("Введите первое значение", ""));
//     this.secondValue = Number(prompt("Веедите второе значение", ""));
//     return this
//   },
//   sum: function () {
//     return this.sumValue = this.firstValue + this.secondValue
//   },
//   mul: function () {
//     return this.mulValue = this.firstValue * this.secondValue
//   }
// };
//
//
// // // calculator.read();
// // console.log(calculator.sum());
// // console.log(calculator.mul());
// console.log(calculator.sumValue); // віведет NaN

//*********************************************************************
// CoffeMachine
/*
        план
        проверить, хватит ли травы и создать объект Кофеварка, в котором:
        10. свойство кофе
        20. свойство вода
            20.1 свойство вода.количество
            20.2 свойство вода.температура
        30. свойство жмых
        40. свойство молоко
        41. cвойство количество приготовленных напитков после очистки от накипи
        42. выбор напитка
        50. приготовление напитка, включая:
            - проверка количества воды, кофе, [молока], температуры воды:
                - если недостаточно какого-то из ингридиентов, вывод сообщения о недостатке этого ингридиента, процесс прерывается
                - если недостаточна температура воды, подогрев до нужной температуры
                - если все условия выполнены, переход к следующему пункту
            - перемалывание нужного количества кофе в зависимости от выбранного напитка
            - добавление нужного количества горячей воды в зависимости от выбранного напитка
            - добавление молока при необходимости
            - вывод напитка пользователю
            - выброс жмыха в контейнер
            - проверка наличия воды, кофе, молока, при отсутствии чего-либо сообщение об этом

        60. метод проверки количества жмыха в контейнере
        70. метод очистки от накипи

        */

let coffeeDrink;

let coffeeMaker = {
    coffee: 0,
    water: {
      waterVolue: 0,
      waterTemp: 20,
    },
    milk: 0,
    waste: 0,
    drinkCounter: 0,
    coffeeKind: {"esspreso":
                    {"coffe": 6,
                     "water": 50,
                     "waste": 26,
                    "milk": 0},
                  "americano":
                      {"coffe": 6,
                      "water": 80,
                        "waste": 26,
                        "milk": 10},
                  "dublesspreso":
                    {"coffe": 12,
                    "water": 100,
                    "waste": 30,
                      "milk": 0},
                  "late":
                    {"coffe": 6,
                      "water": 80,
                      "waste": 26,
                      "milk": 0}},

    setValue: function (ingridient) {

    let requestIngridient = Number(prompt("Add some " + ingridient ));
    if (requestIngridient) {
      console.log(this[ingridient]);
      this[ingridient] += requestIngridient;  // преобразует в this.coffee  в случае с кофе
      console.log(this[ingridient]);
      return true;

    }
    else {
      alert("We cannot make coffee without " + ingridient);
      return false;
    }


    // this.water.waterVolue += Number(prompt("Add some water, ml", ""));
    // this.milk += Number(prompt("Add some milk, ml", ""));
    // this.coffeeKind += Number(prompt("What kind of coffe do you like?", ""));

  },

  makeDrink: function(drinkType){
    this.coffee -= this.coffeeKind[drinkType].coffee;
    this.water.waterVolue -= this.coffeeKind[drinkType].waterVolue;
    this.waste += this.coffeeKind[drinkType].waste;
    this.milk -= this.coffeeKind[drinkType].milk;
    ++this.drinkCounter;
  },



  drinkMaker: function (coffeType) {
    // let qwer = this.setValue();
    console.log(coffeType);

    if (this.coffee < 10) {
      let number = this.setValue("coffee");
      if (!number) {
        return false;
      }
    }

    if (this.water.waterVolue < 200) {
      let number = this.setValue("water");
      if (!number) {
        return false;
      }
    }

    if (this.water.waterVolue < 50) {
      let number = this.setValue("milk");
      if (!number) {
        return false;
      }
    }
  if (this.water > 200){
      alert("Clean the waste container");
      return false;
  }
  if (this.water.waterTemp < 98){
      this.water.waterTemp = 98;
  }
  // длинный вариант с перечислениями

  // if (coffeType == "esspreso"){
  //     this.coffee -= 8;
  //     this.water.waterVolue -= 50;
  //     this.waste += 26;
  //     ++this.drinkCounter;
  //     alert("Your esspreso is done")
  //
  // }
  //   console.log(this);
  //
  //   if (coffeType == "americano"){
  //     this.coffee -= 8;
  //     this.water.waterVolue -= 70;
  //     this.waste += 35;
  //     ++this.drinkCounter;
  //     alert("Your americano is done")
  //   }
  //
  //   if (coffeType == "dublesspreso"){
  //     this.coffee -= 15;
  //     this.water.waterVolue -= 100;
  //     this.waste += 50;
  //     ++this.drinkCounter;
  //     alert("Your dublesspreso is done")
  //   }
  //
  //   if (coffeType == "late"){
  //     this.coffee -= 8;
  //     this.water.waterVolue -= 60;
  //     this.waste += 22;
  //     this.milk -= 10;
  //     ++this.drinkCounter;
  //     alert("Your late is done")
  //   }


    // второй варант
    // if (coffeType == "late"){
    //       this.makeDrink(8, 60, 22);
    //       alert("Your late is done")

    //третий вариант

    this.makeDrink(coffeType);
    alert("Your " + coffeType + "is done")

  },

};
coffeeMaker.drinkMaker(prompt("Select drinks"));














































