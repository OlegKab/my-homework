
//   +++++
// You need to write a function that will return an array of numbers from 1 to 100.
// // But, if a number divides without remnant on 3 - then replace it with the string Fizz.
// //   If a number divides without remnant on 5 - replace it with the string Buzz.
// //   If a number divides without remnant both on 3 and 5 - replace it with the string FizzBuzz.
// //
// //   Example of such array: [1, 2, 'Fizz', 4, 'Buzz', .... , 13, 14, 'FizzBuzz', ...]


// function fizzBuzz () {
//   let result = [];
//
//   for (let i = 1; i <= 100; i++) {
//   result.push(((i % 3 === 0 ) && (i % 5 === 0 )) ? `FizzBuzz` : i && (i % 3 === 0 ) ? `Fizz` : i && (i % 5 === 0 ) ? `Buzz` : i )
//   }
//     return result;
// }
// fizzBuzz();

//*********************************************************************************************

// Math with dynamic-typed values   ----
// Given the function that receives two arguments - number A and number B, add them and return the result.
//   However, number A and number B can be a String representing a number - in
// that case, you need to cast them to a numeric value before you will add them.
//   If one or both of provided values are not a Number and not a String that can be cast to a number - return false instead.
//
//   For example, addNumbers("2", 2) will return a number 4.

// function addNumbers(numA, numB) {
//   if (numbers !==0){
//
//   }
//
//   let sum = (parseInt(numA) + parseInt(numB));
//   return sum;
//
// console.log(sum);
// }


// function addNumbers(numA, numB) {
//
//   if (numA instanceof Number && numB instanceof Number){
//     console.log(Number(numA) + Number(numB))
//
//   }
//   return false
// }
//
// let addNumbers = ["2", 2];
//  addNumbers();



 //*********************************************************************************************

// Random dice   ++++
// Create a function that will return a random integer between 1 and 6. Use Math.random to generate a random number.
//
//   For example, randomDice() may return 3 (but number needs to be random every time, not preset).


//
// function randomDice() {
//  let rand = Math.floor(Math.random()*6+1);
//  return rand;
// }
//
// console.log(randomeDice());


//***********************************************************************************************

// Binary to decimal   +++
// Write a function that will receive a binary number as a string (like "111000111") and will return its decimal equivalent as an integer.

// For example, bin2Dec("11110") will return number 30.

// function bin2Dec(bin) {
//   return( parseInt( bin, 2 ) )
// }


//************************************************************************************************

// Decimal to binary   +++
// Write a function that will receive a decimal number and will return its binary equivalent as a string (like "11011").
//
// For example, dec2Bin(123) will return string 1111011.
//
// function dec2Bin(dec) {
//   return (dec).toString(2)
// }

//**********************************************************************************************

// Natural number  +++
// Check that provided number is a natural, ie numbers like 0, 1, 2, 3... but not 0.42, 1.2, 10.5, etc. Return boolean value.
//
//   For example, isNatural(123) will return boolean true.
//
// function isNatural(n) {
//   return (n ^ 0) === n;
// }
// console.log(isNatural(2.2));


//************************************************************************************************

// Sum of values in an array  +++
// Given an array of mixed values, write a function that will accept
// such array as an argument and will return a sum of all numeric values in the array.
//   If a string contains number - it must be cast to it and calculated as well.
//   Non-number values and string that can't be cast to a number must be ignored.
//
// For example, sumNumbers([1, "2", "abc", 3]) must return number 6.

// function sumNumbers(arr){
//   let result = 0;
//   for (var i = 0; i < arr.length; i++) {
//     result += +arr[i];
//   }
//   return result
// }

// sumNumbers ([1, "2", "abc", 3]);

//***********************************************************************************************

// Round a number  ----
// Write a function that will round a provided number to a specified number of a provided digits.
//   So a number 123.123123 with param 2 must return 123.12. Return a number.
//   Use parseFloat and toFixed methods.
//
//   For example, roundNumber(123.59, 1) will return a number 123.6.

// function roundNumber(number, digits) {
//   let round = number.toFixed(digits);
//   console.log(round);
//   return round
// }
// roundNumber(123.59, 1);







//************************************************************************************************
//************************************************************************************************
// Working with strings in Javascript
//   String Object
//
// Characters in a string can be processed as an array
// Array Object
//
// Method replace may be used to replace one or more characters in a string.
// "abc".replace("a", "d")- will return string dbc.
//
//   Method indexOf can return position of a substring in a string (starting from 0). It will return -1 if string dont contains substring.
// "abc".indexOf("c") - will return number 2.
//
// Methods toLowerCase and toUpperCase can change letter register.
// "abc".toUpperCase() - will return a string "abc".
// "ABC".toLowerCase() - will return a string "ABC".
//
//   Method substring can return a substring based on two indexes.
// "hello world".substring(0, 5) - will return a string hello.

//************************************************************************************************
//************************************************************************************************

// Values concatenation   ++++
// Write a function that will receive two arguments and will return the result of a concatenation of their string value.
//
//   For example, concate("ab", "c") should return "abc", concate(2, 2) should return "22".




// function concate(valueA, valueB) {
//   result = (String(valueA) + String(valueB));
//   return result
//
// }
// concate("6", "2");

//**************************************************************************************************

// String contains   -----
// Create a function that will receive a string A and a string B.
//   Return true if string A contains in it string B, or false if not.
//   Make comparison non-case sensitive.
//
//   For example, contains("Hello World!", "world) should return true.

// function contains(str, subString) {
//     let str = "Hello World!";
//   if (typeof str == String && typeof subString === String) {
//     console.log('true')
//   }
// }


//***************************************************************************************************

// Find the index of the substring  +++
// Write a function that will receive two strings - string A and string B.
//   Find the index of string B position inside string A.
//   If string A doesn't contain string B - return false.
//
// For example, findIndex("abc", "bc") should return 1.


// function findIndex(str, subString) {
//
//   if (str.indexOf(subString) == -1){
//     return false
//   } else {
//     return str.indexOf(subString)
//   }
// }



//***************************************************************************************************

// Search and modify characters in a string  ----
// Write a function that will receive a string and a character as arguments.
// Search string for all occurrences of provided character and capitalize it.
//
// For example, showCharacter("abc", "b") should return "aBc".

// function showCharacter(str, chr) {
//
//   str.indexOf(chr)
//
// }
//
// var str = 'Быть или не быть, вот в чём вопрос.';
// var count = 0;
// var pos = str.indexOf('в');
//
// while (pos !== -1) {
//   count++;
//   pos = str.indexOf('в', pos + 1);
// }
//
// console.log(count); // отобразит 3

//***************************************************************************************************

// Short name
// Write a function that will receive a name as an argument and will return its short form.
//   So a name like John Adams will be formatted as John A.
//
//   For example, abbrName("Richard Mabey") will return Richard M.

// function abbrName (str) {
//
// return
//
//
// }



//***************************************************************************************************

// Capitalize words  +++++
// Write a function that will receive a string and will return it with all words being capitalized.
//   So a hello world will be returned as Hello World.
//
//   For example, capitalizeWords("capitalize words") will return Capitalize Words.

// function capitalizeWords(str) {
//   return str.replace(/\b\w/g, l => l.toUpperCase())
// }


//***************************************************************************************************

// camelCase   ++++
// Write a function that will camelize provided string.
//   So string "hello world" will become "helloWorld".
//   The first letter must remain in lower case.
//
// For example, camelCase("java script") will return javaScript.

// function camelCase(str) {
//   str = str.toLowerCase().replace(/(?:(^.)|([-_\s]+.))/g, function(match) {
//     return match.charAt(match.length-1).toUpperCase();
//   });
//   return str.charAt(0).toLowerCase() + str.substring(1);
// }


//***************************************************************************************************



//
// function makeFirstSymbolUpperCase(arrayList) {
//   let newArray = arrayList[0].toUpperCase() + arrayList.slice(1);
//   return newArray
//
// }
//
// alert(makeFirstSymbolUpperCase("вася"));



//***************************************************************************************************
//***************************************************************************************************

//***************************************************************************************************
//***************************************************************************************************












